import functools
import math
from typing import Union

import numpy as np
import torch
from cocoex.interface import Problem
from torch import Tensor
from torch.types import Device

from space.base_space import (
    BudgetLimitedSpace,
    SamplerSpace,
    EvaluatedSpace,
    BoundedSpace,
    TData,
)
from space.exceptions import NoMoreBudgetError


def is_multiple_points(data: Union[Tensor, np.ndarray]) -> bool:
    return len(data.shape) == 2


class CocoSpace(BudgetLimitedSpace, SamplerSpace, EvaluatedSpace, BoundedSpace):
    def __init__(self, coco_func: Problem, budget: int = math.inf):
        self.coco_func = coco_func
        self.budget = budget
        self.num_of_samples = 0

    def __call__(self, data_from_input_space: TData, debug_mode: bool = False) -> TData:
        if not debug_mode:
            self._pre_evaluation_action(
                len(data_from_input_space) if is_multiple_points(data_from_input_space) else 1
            )
        return self.__evaluate_env(data_from_input_space)

    def initialize(self):
        self.num_of_samples = 0

    @functools.singledispatchmethod
    def __evaluate_env(self, data):
        raise NotImplementedError()

    @__evaluate_env.register(np.ndarray)
    def call_with_ndarray(self, data: np.ndarray) -> np.ndarray:
        if is_multiple_points(data):
            calculated_points = [self.coco_func(point) for point in data]
        else:
            calculated_points = self.coco_func(data)
        return np.array(calculated_points)

    @__evaluate_env.register(Tensor)
    def call_with_tensor(self, data: Tensor) -> Tensor:
        if is_multiple_points(data):
            calculated_points = [self.coco_func(point) for point in data.cpu()]
        else:
            calculated_points = self.coco_func(data)
        return torch.tensor(calculated_points, device=data.device)

    @property
    def total_budget(self) -> int:
        return self.budget

    @property
    def used_budget(self) -> int:
        return self.num_of_samples

    def sample_for_optimum_points(
        self, budget: int, num_of_samples: int, device: Device = torch.device("cpu")
    ) -> Tensor:
        samples = {point: self.coco_func(point) for point in self.sample_from_space(budget)}
        min_point_value = sorted(samples.items(), key=lambda x: x[1])[:num_of_samples]
        return torch.tensor([list(point[0]) for point in min_point_value], device=device)

    def sample_from_space(
        self, num_samples: int, device: Device = torch.device("cpu")
    ) -> Tensor:
        size_of_possible_input = (self.upper_bound - self.lower_bound).to(device=device)
        return (
            torch.rand(num_samples, self.coco_func.dimension, device=device)
            * size_of_possible_input
        ) + self.lower_bound.to(device=device)

    def best_k_values(self, input_in_space: Tensor, k: int, debug_mode: bool = False) -> Tensor:
        new_x_opt_env_value = self(input_in_space, debug_mode=debug_mode)
        top_k_new_x_opt_indices = (-new_x_opt_env_value).topk(k).indices

        return input_in_space[top_k_new_x_opt_indices]

    def _pre_evaluation_action(self, num_of_eval: int):
        self.num_of_samples += num_of_eval
        if self.num_of_samples > self.budget:
            raise NoMoreBudgetError(f"Exceeded budget of f{self.budget}")

    def denormalize(self, data: Tensor) -> Tensor:
        return 0.5 * (data + 1) * (self.upper_bound - self.lower_bound).to(
            device=data.device
        ) + self.lower_bound.to(device=data.device)

    @functools.cached_property
    def upper_bound(self):
        return torch.from_numpy(self.coco_func.upper_bounds).to(dtype=torch.float32)

    @functools.cached_property
    def lower_bound(self):
        return torch.from_numpy(self.coco_func.lower_bounds).to(dtype=torch.float32)

    def __str__(self):
        return f"{self.__repr__()}, remaining budget: {self.num_of_samples}"

    def __repr__(self):
        return f"coco {self.coco_func.id_function}: {self.coco_func.dimension} - {self.coco_func.id_instance}"