from typing import TypeVar, Protocol, runtime_checkable

import numpy as np
import torch
from torch import Tensor
from torch.types import Device

TData = TypeVar("TData", Tensor, np.ndarray)


@runtime_checkable
class BudgetLimitedSpace(Protocol):
    @property
    def used_budget(self) -> int:
        raise NotImplementedError()

    @property
    def total_budget(self) -> int:
        raise NotImplementedError()

    def initialize(self):
        raise NotImplementedError()


@runtime_checkable
class EvaluatedSpace(Protocol):
    def __call__(self, data_from_input_space: TData, debug_mode: bool = False) -> TData:
        raise NotImplementedError()


@runtime_checkable
class BoundedSpace(Protocol):
    @property
    def upper_bound(self) -> Tensor:
        raise NotImplementedError()

    @property
    def lower_bound(self) -> Tensor:
        raise NotImplementedError()

    def denormalize(self, data: Tensor) -> Tensor:
        """
        map data from (-1,1) to entire space
        """
        raise NotImplementedError()


@runtime_checkable
class SamplerSpace(Protocol):
    """
    This class is a space that allows user to sample from it
    """

    def sample_for_optimum_points(
        self, budget: int, num_of_samples: int, device: Device = torch.device("cpu")
    ) -> Tensor:
        """
        sample the space and find n best points (with optimum values)
        :param budget: The number of time to sample from space
        :param num_of_samples: Number of best sample to return
        :param device: In which device to return the samples
        """
        raise NotImplementedError()

    def sample_from_space(
        self, num_samples: int, device: Device = torch.device("cpu")
    ) -> Tensor:
        """
        Sample from Omega space i.e. from the input space of the loss function f
        :param num_samples: number of samples to take from the space
        :param device: Which processor to use
        """
        raise NotImplementedError()

    def best_k_values(self, input_in_space: Tensor, k: int, debug_mode: bool = False) -> Tensor:
        """
        Find the k best points in the space
        :param input_in_space: list of points in the space
        :param k: The num of top result you want to get
        :param debug_mode: Weather or not this is used for debugging
        """
        raise NotImplementedError()
