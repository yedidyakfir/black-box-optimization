import functools
import math
from typing import Callable

import numpy as np
import torch
from torch import Tensor
from torch.types import Device

from space.base_space import (
    BoundedSpace,
    EvaluatedSpace,
    SamplerSpace,
    BudgetLimitedSpace,
    TData,
)
from space.coco import is_multiple_points
from space.exceptions import NoMoreBudgetError


class CallableSpace(BudgetLimitedSpace, SamplerSpace, EvaluatedSpace, BoundedSpace):
    def __init__(
        self,
        func: Callable,
        input_lower_bounds,
        input_upper_bounds,
        dims: int,
        func_name: str,
        budget: int = math.inf,
    ):
        self.func = func
        self.input_upper_bounds = input_upper_bounds
        self.input_lower_bounds = input_lower_bounds
        self.dims = dims
        self.func_name = func_name
        self.budget = budget
        self.num_of_samples = 0

    def __repr__(self):
        return f"{self.func_name}-{self.dims}"

    def __str__(self):
        return f"{self.__repr__()}, remaining budget: {self.num_of_samples}"

    @property
    def total_budget(self) -> int:
        return self.budget

    @property
    def used_budget(self) -> int:
        return self.num_of_samples

    def initialize(self):
        self.num_of_samples = 0

    def sample_for_optimum_points(
        self, budget: int, num_of_samples: int, device: Device = torch.device("cpu")
    ) -> Tensor:
        samples = {point: self.func(point) for point in self.sample_from_space(budget)}
        min_point_value = sorted(samples.items(), key=lambda x: x[1])[:num_of_samples]
        return torch.tensor([list(point[0]) for point in min_point_value], device=device)

    def sample_from_space(
        self, num_samples: int, device: Device = torch.device("cpu")
    ) -> Tensor:
        size_of_possible_input = (self.input_upper_bounds - self.input_lower_bounds).to(
            device=device
        )
        return (
            torch.rand(num_samples, self.dims, device=device) * size_of_possible_input
        ) + self.input_lower_bounds.to(device=device)

    def best_k_values(self, input_in_space: Tensor, k: int, debug_mode: bool = False) -> Tensor:
        new_x_opt_env_value = self(input_in_space, debug_mode=debug_mode)
        top_k_new_x_opt_indices = (-new_x_opt_env_value).topk(k).indices

        return input_in_space[top_k_new_x_opt_indices]

    def _pre_evaluation_action(self, num_of_eval: int):
        self.num_of_samples += num_of_eval
        if self.num_of_samples > self.budget:
            raise NoMoreBudgetError(f"Exceeded budget of f{self.budget}")

    def __call__(self, data_from_input_space: TData, debug_mode: bool = False) -> TData:
        if not debug_mode:
            self._pre_evaluation_action(
                len(data_from_input_space) if is_multiple_points(data_from_input_space) else 1
            )
        return self.__evaluate_env(data_from_input_space)

    @functools.singledispatchmethod
    def __evaluate_env(self, data):
        raise NotImplementedError()

    @__evaluate_env.register(np.ndarray)
    def call_with_ndarray(self, data: np.ndarray) -> np.ndarray:
        if is_multiple_points(data):
            calculated_points = [self.func(point) for point in data]
        else:
            calculated_points = self.func(data)
        return np.array(calculated_points)

    @__evaluate_env.register(Tensor)
    def call_with_tensor(self, data: Tensor) -> Tensor:
        if is_multiple_points(data):
            calculated_points = [self.func(point) for point in data.cpu()]
        else:
            calculated_points = self.func(data)
        return torch.tensor(calculated_points, device=data.device)

    @property
    def upper_bound(self):
        return self.input_upper_bounds

    @property
    def lower_bound(self):
        return self.input_lower_bounds

    def denormalize(self, data: Tensor) -> Tensor:
        return 0.5 * (data + 1) * (self.input_upper_bounds - self.input_lower_bounds).to(
            device=data.device
        ) + self.input_lower_bounds.to(device=data.device)
