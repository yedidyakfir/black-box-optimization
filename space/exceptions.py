class SpaceError(Exception):
    pass


class NoMoreBudgetError(SpaceError):
    pass
