# Black Box Optimization

BBO

# How to run the project
## Clone
```
cd existing_repo
git clone https://gitlab.com/yedidyakfir/black-box-optimization.git
```

Please Note that some of the packages may be incompatible with python 3.10
## install requirements
Then install pytorch
```
conda install pytorch -c pytorch
```

Then install gpu support
```
conda install pytorch cudatoolkit -c pytorch
```


The requirements are in the requirements file
```
pip install -r requirements.txt
```

Also install scipy


<code>conda install scipy</code> for conda <code>pip install scipy</code> for python

## Use coco environments
To use the coco test suite you need install cocopp
the requirements file already contains the package but you need a few more steps to get the full package

- clone the repo
```git clone https://github.com/numbbo/coco.git```
- enter the folder ```cd coco```
- run the following command ```python do.py run-python```

## Add support to progress bar
To see the progress bar in pycharm 
- Open the run configuration on the top right
- enable 'evaluate terminal in output console'

![img.png](img.png)

## Test and Deploy
To run the tests
```
python -m pytest tests
```
***
