DEFAULT_OPT_GAN_CONFIG = {
    "g_factor": 0.01,
    "d_iter": 4,
    "g_iter": 150,
    "ee_factor": 0.3,
    "batch_size": 30,
    "database_size": 200,
    "pre_iter_loops": 20,
}

OPT_GAN_CONFIG = {
    "g_factor": {"values": [0.01, 0.1, 0.2, 0.3]},
    "d_iter": {"values": [4, 6, 10]},
    "g_iter": {"values": [150, 200, 250]},
    "ee_factor": {"values": [0.3, 0.4, 0.2]},
    "batch_size": {"values": [256, 512, 1024]},
    "database_size": {"values": [200, 500, 1000]},
    "pre_iter_loops": {"values": [10, 20, 40]},
}


DEFAULT_EGL_CONFIG = {
    "eps": 0.316227766016838,
    "eps_factor": 0.97,
    "min_eps": 1e-4,
    "perturb": 0,
    "output_outlier": 0.1,
    "batch_size": 64,
    "explore_size": 40,
}

DEFAULT_IGL_CONFIG = {
    "eps": 0.316227766016838,
    "eps_factor": 0.97,
    "min_eps": 1e-4,
    "perturb": 0,
    "output_outlier": 0.1,
    "output_lr": 0.3,
    "batch_size": 64,
    "explore_size": 40,
}
