import logging

from cg_main import run_cg
from cma_main import run_cma
from config import DEFAULT_EGL_CONFIG, DEFAULT_IGL_CONFIG, DEFAULT_OPT_GAN_CONFIG
from egl_main import run_egl
from igl_main import run_igl
from opt_gan_main import run_opt_gan
from utils import make_dataclass_from_dict, spaces_index_to_run

for space in spaces_index_to_run():
    output_graph = False
    logging.basicConfig(format="%(levelname)s - %(message)s", level=logging.INFO)
    space.initialize()
    run_opt_gan(
        space,
        make_dataclass_from_dict(DEFAULT_OPT_GAN_CONFIG),
        output_bar=False,
        output_graph=output_graph,
    )
    space.initialize()
    run_cma(space, None, output_bar=False, output_graph=output_graph)
    space.initialize()
    run_cg(space, None, output_bar=False, output_graph=output_graph)
    space.initialize()
    run_egl(
        space,
        make_dataclass_from_dict(DEFAULT_EGL_CONFIG),
        output_bar=False,
        output_graph=output_graph,
    )
    space.initialize()
    run_igl(
        space,
        make_dataclass_from_dict(DEFAULT_IGL_CONFIG),
        output_bar=False,
        output_graph=output_graph,
    )
