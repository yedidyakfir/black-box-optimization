import functools
import logging
from dataclasses import dataclass
from typing import Union

import torch
from torch.types import Device
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

from config import DEFAULT_OPT_GAN_CONFIG
from convergence_algorithms.modules import DiscriminatorByInputSize, GeneratorByDimSize
from convergence_algorithms.opt_gan import OptGAN
from convergence_algorithms.typing import BoundedEvaluatedSamplerSpace
from handlers.drawer_handlers import TensorboardDrawerHandler
from handlers.drawers.base_drawer import MultipleDrawerEpoch
from handlers.drawers.contour_disc_gan import (
    DiscriminatorContourDrawer,
    Discriminators,
    GanDistributionDrawer,
)
from handlers.drawers.convergence_drawer import FullFigConvergenceDrawer, SamplesDrawer
from utils import (
    spaces_index_to_run,
    make_dataclass_from_dict,
    find_the_most_free_device,
    create_global_handlers,
)


def run_opt_gan(
    space: BoundedEvaluatedSamplerSpace,
    config: dataclass,
    device: Union[Device, int] = 0,
    output_bar: bool = True,
    output_graph: bool = True,
):
    dims = len(space.lower_bound)
    env_name = repr(space)

    explore_disc = DiscriminatorByInputSize(dims).to(device=device)
    exploit_disc = DiscriminatorByInputSize(dims).to(device=device)
    generator = GeneratorByDimSize(dims).to(device=device)
    best_database_samples = space.sample_for_optimum_points(1000, 200, device=device)
    opt_gan = OptGAN(
        space,
        generator,
        explore_disc,
        exploit_disc,
        gradient_penalty_factor=config.g_factor,
        discriminator_iter=config.d_iter,
        generator_iter=config.g_iter,
        ee_factor=config.ee_factor,
        best_dataset_samples=best_database_samples.clone(),
        device=device,
    )
    writer = SummaryWriter(filename_suffix="opt_gan", comment="opt_gan")

    if not output_bar:
        tqdm.__init__ = functools.partialmethod(tqdm.__init__, disable=True)

    opt_gan.train(
        pre_iter_loops=config.pre_iter_loops,
        batch_size=config.batch_size,
        epochs=int(50_000 / config.database_size),
        k_0=int(config.database_size * 0.75),
        callback_handlers=[
            *create_global_handlers("opt_gan", env_name, writer=writer),
            *(
                [
                    TensorboardDrawerHandler(
                        DiscriminatorContourDrawer(
                            space,
                            [torch.linspace(-5, 5, 100)] * dims,
                            Discriminators.EXPLOITATION,
                        ),
                        name=f"opt_gan_{env_name}",
                        writer=writer,
                    ),
                    TensorboardDrawerHandler(
                        DiscriminatorContourDrawer(
                            space,
                            [torch.linspace(-5, 5, 100)] * dims,
                            Discriminators.EXPLORATION,
                        ),
                        name=f"opt_gan_{env_name}",
                        writer=writer,
                    ),
                    TensorboardDrawerHandler(
                        MultipleDrawerEpoch(
                            [
                                FullFigConvergenceDrawer(dim_size=dims, map_output=False),
                                SamplesDrawer(),
                            ]
                        ),
                        name=f"opt_gan_{env_name} function",
                        writer=writer,
                    ),
                    TensorboardDrawerHandler(
                        GanDistributionDrawer([torch.linspace(-5, 5, 10)] * dims),
                        name=f"opt_gan {env_name} gan dist",
                        writer=writer,
                    ),
                ]
                if output_graph
                else []
            ),
        ],
    )


def main():
    logging.basicConfig(format="%(levelname)s - %(message)s", level=logging.INFO)
    for space in spaces_index_to_run():
        run_opt_gan(
            space,
            make_dataclass_from_dict(DEFAULT_OPT_GAN_CONFIG),
            find_the_most_free_device(),
            output_bar=False,
            output_graph=False,
        )


if __name__ == "__main__":
    main()
