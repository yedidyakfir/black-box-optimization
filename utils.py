import dataclasses
from typing import Iterable, List, Type, Dict, Any

import numpy
import torch.cuda
from OptimizationTestFunctions import (
    Michalewicz,
    SchwefelSin,
    Transformation,
    Weierstrass,
    Sphere,
    Rastrigin,
)
from aenum import Enum
from cocoex import Suite
from torch.types import Device

from convergence_algorithms.typing import BoundedEvaluatedSamplerSpace
from handlers.base_handler import AlgorithmCallbackHandler
from handlers.drawer_handlers import TensorboardDrawerHandler, DrawerHandler
from handlers.drawers.budget_drawers import BestPerBudget, CurrentValuePerBudget
from handlers.drawers.utils import convert_to_real_drawer
from handlers.save_run import SaveRunToFileHandler, SaveMinMaxHandler
from space.callable_function import CallableSpace
from space.coco import CocoSpace

WANDB_PROJECT_NAME = "black-box-optimization"


def spaces_index_to_run() -> Iterable[BoundedEvaluatedSamplerSpace]:
    def funcs(dim):
        return [
            CallableSpace(
                Michalewicz(),
                torch.tensor([5] * dim),
                torch.tensor([-5] * dim),
                dim,
                "michalewicz",
                150_000,
            ),
            CallableSpace(
                Transformation(
                    SchwefelSin(dim), shift_step=numpy.array([2] * dim), rotation_matrix=2
                ),
                torch.tensor([5] * dim),
                torch.tensor([-5] * dim),
                dim,
                "Shifted Rotated SchwefelSin",
                150_000,
            ),
            CallableSpace(
                Transformation(
                    Weierstrass(dim), shift_step=numpy.array([2] * dim), rotation_matrix=2
                ),
                torch.tensor([5] * dim),
                torch.tensor([-5] * dim),
                dim,
                "Shifted Rotated Weierstrass",
                150_000,
            ),
            CallableSpace(
                Sphere(dim),
                torch.tensor([5] * dim),
                torch.tensor([-5] * dim),
                dim,
                "Sphere",
                150_000,
            ),
            CallableSpace(
                Rastrigin(dim),
                torch.tensor([5] * dim),
                torch.tensor([-5] * dim),
                dim,
                "Sphere",
                150_000,
            ),
            coco_space_from_funcnum_and_dim(7, dim)
        ]

    dims = 2
    dims_2 = 10

    return [
        *funcs(dims),
        *funcs(dims_2),
    ]
    return (coco_space_from_index(i) for i in range(1200, 2000, 10))


class Algorithms(Enum):
    EGL = "egl"
    IGL = "igl"
    CG = "cg"
    OPT_GAN = "opt_gan"
    CMA = "cma"


def find_the_most_free_device() -> Device:
    if not torch.cuda.is_available():
        return torch.device("cpu")
    maximum_available_gpu = 0
    most_available_gpu_index = 0
    for device_num in range(torch.cuda.device_count()):
        available_memory, _ = torch.cuda.mem_get_info(device_num)
        if available_memory > maximum_available_gpu:
            most_available_gpu_index = device_num
    return torch.device(most_available_gpu_index)


def float_range(first: float, second: float = None, skip: float = 1) -> Iterable[int]:
    i = first if second else 0
    max_number = second if second else first
    while i < max_number:
        yield i
        i += skip


def create_global_handlers(
    alg_name: str,
    env_name: str,
    convert_to_real: bool = False,
    writer_handler: Type[DrawerHandler] = TensorboardDrawerHandler,
    save_to_files: bool = True,
    **addition_params_for_handler,
) -> List[AlgorithmCallbackHandler]:
    return [
        *(
            [
                SaveRunToFileHandler(
                    convert_to_real_drawer(BestPerBudget())
                    if convert_to_real
                    else BestPerBudget(),
                    alg_name=alg_name,
                    env_name=env_name,
                ),
                SaveMinMaxHandler(env_name=env_name),
            ]
            if save_to_files
            else []
        ),
        writer_handler(
            convert_to_real_drawer(BestPerBudget()) if convert_to_real else BestPerBudget(),
            name=env_name,
            **addition_params_for_handler,
        ),
        writer_handler(
            convert_to_real_drawer(CurrentValuePerBudget())
            if convert_to_real
            else CurrentValuePerBudget(),
            name=env_name,
            **addition_params_for_handler,
        ),
    ]


def make_dataclass_from_dict(config: Dict[str, Any]):
    config_class = dataclasses.make_dataclass(
        "Config",
        [(config_name, type(config_value)) for config_name, config_value in config.items()],
    )
    return config_class(**config)


def coco_space_from_index(index: int) -> CocoSpace:
    f1 = Suite("bbob", "", "")[index]
    return CocoSpace(f1, 150_000)


def coco_space_from_funcnum_and_dim(func_num: int, dim: int) -> CocoSpace:
    benchmark = Suite("bbob", "", "")
    f1 = [func for func in benchmark if func.id_function == func_num and func.dimension == dim]
    f1 = f1[0]
    return CocoSpace(f1, 150_000)
