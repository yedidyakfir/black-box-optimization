import os
from argparse import ArgumentParser
from pathlib import Path
from typing import Tuple, List

from torch.utils.tensorboard import SummaryWriter

from handlers.save_run import MeanResultCalculater
from utils import Algorithms


def log_result(mean_result: List[Tuple[float, int]], alg_name: str, step_size: int):
    writer = SummaryWriter(filename_suffix=alg_name, comment=alg_name)

    for result in mean_result:
        writer.add_scalar(
            f"mean result step size: {step_size}", result[0], global_step=result[1]
        )


def calc_mean_result_alg(
    algorithm: Algorithms, result_path: Path, min_max_path: Path, step_size: int
):
    mean_calculator = MeanResultCalculater(result_path, min_max_path, algorithm.value)
    mean_result = sorted(mean_calculator.calc_mean(), key=lambda budget_mean: budget_mean[1])
    log_result(mean_result, algorithm.value, step_size)


def main():
    args_parser = ArgumentParser()
    args_parser.add_argument(
        "-a",
        "--algorithm",
        type=Algorithms,
        default=None,
        help="Which algorithm to check?",
    )
    args_parser.add_argument(
        "-r",
        "--results_path",
        default=Path(os.getcwd()) / "results" / "budget",
        type=Path,
        help="The location of the algorithm result",
    )
    args_parser.add_argument(
        "-m",
        "--min_max_path",
        type=Path,
        default=Path(os.getcwd()) / "results" / "min_max",
        help="The location of the min and max result of the space",
    )
    args_parser.add_argument(
        "-s",
        "--step_size",
        type=int,
        default=100,
        help="How many step will be grouped together (i.e. budget: 1 and 2 will be in the same bar in the graph)",
    )
    args = args_parser.parse_args()
    if not args.algorithm:
        for alg in list(Algorithms):
            calc_mean_result_alg(alg, args.results_path, args.min_max_path, args.step_size)
    else:
        calc_mean_result_alg(
            args.algorithm, args.results_path, args.min_max_path, args.step_size
        )


if __name__ == "__main__":
    main()
