import functools
import logging
from dataclasses import dataclass
from typing import Union

from torch.nn import Sequential, Linear, SmoothL1Loss, ReLU
from torch.optim import SGD, Adam
from torch.types import Device
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

from config import DEFAULT_IGL_CONFIG
from convergence_algorithms.igl import IGL
from convergence_algorithms.modules import BaseSequentialModel
from convergence_algorithms.typing import BoundedEvaluatedSpace
from handlers.drawer_handlers import TensorboardDrawerHandler
from handlers.drawers.base_drawer import MultipleDrawerEpoch
from handlers.drawers.convergence_drawer import (
    BestModelDrawer,
    FullFigConvergenceDrawer,
    SamplesDrawer,
    StudiedFunctionConvergenceDrawer,
)
from handlers.drawers.draw_regions import TrustRegionDrawer
from handlers.drawers.utils import convert_to_real_drawer
from mapping.unconstrained_to_real import (
    ShrinkingRegionMapping,
    AdaptedOutputUnconstrainedMapping,
)
from utils import (
    find_the_most_free_device,
    spaces_index_to_run,
    create_global_handlers,
    make_dataclass_from_dict,
)


def run_igl(
    space: BoundedEvaluatedSpace,
    config: dataclass,
    device: Union[Device, int] = 0,
    output_bar: bool = True,
    output_graph: bool = True,
):
    writer = SummaryWriter(filename_suffix="igl", comment="igl")

    dims = len(space.lower_bound)

    grad_net = Sequential(Linear(dims, 10), ReLU(), Linear(10, 15), ReLU(), Linear(15, 1)).to(
        device=device
    )
    model_net = Sequential(Linear(dims, 1, bias=False)).to(device=device)
    igl = IGL(
        space,
        grad_net,
        BaseSequentialModel(model_net),
        Adam(
            grad_net.parameters(),
            lr=0.001,
            betas=(0.9, 0.999),
            eps=1e-04,
        ),
        SGD(model_net.parameters(), lr=0.01),
        epsilon=config.eps,
        epsilon_factor=config.eps_factor,
        min_epsilon=config.min_eps,
        perturb=config.perturb,
        loss=SmoothL1Loss(),
        output_mapping=AdaptedOutputUnconstrainedMapping(
            config.output_outlier, lr=config.output_lr
        ),
        input_mapping=ShrinkingRegionMapping((dims,), device=device),
        device=device,
    )

    if not output_bar:
        tqdm.__init__ = functools.partialmethod(tqdm.__init__, disable=True)

    igl.train(
        300,
        config.batch_size,
        config.explore_size,
        callback_handlers=[
            *create_global_handlers("igl", repr(space), True, writer=writer),
            *(
                [
                    TensorboardDrawerHandler(
                        MultipleDrawerEpoch(
                            [
                                convert_to_real_drawer(FullFigConvergenceDrawer(dim_size=dims)),
                                convert_to_real_drawer(BestModelDrawer()),
                                convert_to_real_drawer(TrustRegionDrawer(dim_size=dims)),
                                convert_to_real_drawer(SamplesDrawer()),
                            ]
                        ),
                        name=f"igl {repr(space)}",
                        writer=writer,
                    ),
                    TensorboardDrawerHandler(
                        MultipleDrawerEpoch(
                            [
                                convert_to_real_drawer(
                                    StudiedFunctionConvergenceDrawer(
                                        dim_size=dims, map_output=False
                                    )
                                ),
                                convert_to_real_drawer(BestModelDrawer()),
                                convert_to_real_drawer(TrustRegionDrawer(dim_size=dims)),
                                convert_to_real_drawer(SamplesDrawer()),
                            ]
                        ),
                        name=f"Learned func igl: {repr(space)}",
                        writer=writer,
                    ),
                ]
                if output_graph
                else []
            ),
        ],
    )


def main():
    logging.basicConfig(format="%(levelname)s - %(message)s", level=logging.INFO)
    for space in spaces_index_to_run():
        run_igl(
            space,
            make_dataclass_from_dict(DEFAULT_IGL_CONFIG),
            find_the_most_free_device(),
            output_bar=False,
            output_graph=False,
        )


if __name__ == "__main__":
    main()
