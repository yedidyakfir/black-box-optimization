import torch
from torch import Tensor

from mapping.base import InputMapping, OutputMapping


class InputUnconstrainedMapping(InputMapping):
    def __init__(self, lower_bound: int, upper_bound: int):
        self.m = 2 / (upper_bound - lower_bound)

    def map(self, tensor: Tensor):
        return torch.arctanh(self.m * tensor)

    def inverse(self, tensor: Tensor):
        return (1 / self.m) * torch.tanh(tensor)


class ShrinkingRegionMapping(InputMapping):
    def __init__(
        self,
        shape: tuple,
        shrink_factor: float = 0.9,
        min_sigma: float = 0.1,
        device: torch.device = torch.device("cpu"),
    ):
        self.mu = torch.zeros(shape, device=device)
        self.sigma = torch.ones(shape, device=device)
        self.shrink_factor = shrink_factor
        self.min_sigma = min_sigma * torch.ones(shape, device=device)

    def map(self, tensor: Tensor):
        tensor = torch.clamp(tensor, min=-1 + 1e-5, max=1 - 1e-5)
        return torch.arctanh((tensor - self.mu) / self.sigma.view(1, -1))

    def inverse(self, tensor: Tensor):
        tensor = self.mu + self.sigma * torch.tanh(tensor)
        tensor = torch.clamp(tensor, min=-1, max=1)
        return tensor

    def squeeze(self, best_result: Tensor):
        for i in range(len(best_result)):
            # Check if the best result has passed the threshold for shrinking
            if (
                best_result[i] < self.mu[i] + (1 - self.min_sigma[i]) * self.sigma[i]
                or best_result[i] > self.mu[i] - (1 - self.min_sigma[i]) * self.sigma[i]
            ):
                self.sigma[i] = self.shrink_factor * self.sigma[i]
        self.mu = best_result.detach()


class OutputUnconstrainedMapping(OutputMapping):
    def __init__(self, outlier: float, epsilon: float = 1e-5):
        """
        This class is mapping using 2 functions
        1.  linear function that with these points (Q[outlier], -1), (Q[1-outlier], 1)
            Q[outlier] is the outlier percentile of the data
        2.  log function (depends on the output of the first function)
        :param epsilon: infinitesimal number to avoid dividing by zero
        """
        self.outlier = outlier
        self.epsilon = epsilon

    def map(self, x: Tensor):
        data_size = len(x)
        outlier = int(data_size * self.outlier)
        x1, _ = torch.kthvalue(x, outlier)
        x2, _ = torch.kthvalue(x, data_size - outlier)
        median = torch.median(x)

        m = 2 / (x2 - x1 + self.epsilon)
        output = x * m + median

        # Note: When x is positive the negative log value of x will be nan, this will prevent errors like these
        x_clamp_up = torch.clamp(x, min=1e-5)
        x_clamp_down = torch.clamp(x, max=-1e-5)

        # Squashing
        x_log_up = torch.log(x_clamp_up) + 1
        x_log_down = -torch.log(-x_clamp_down) - 1
        return (
            x_log_up * (output >= 1).float()
            + output * (output >= -1).float() * (output < 1).float()
            + x_log_down * (output < -1).float()
        )

    def inverse(self, x: Tensor):
        raise NotImplementedError("I dont remember m and median")


class AdaptedOutputUnconstrainedMapping(OutputMapping):
    def __init__(
        self,
        outlier: float,
        lr: float = 0.1,
        epsilon: float = 1e-5,
        squash_eps: float = 1e-5,
        y1: float = -1,
        y2: float = 1,
    ):
        """
        This class is mapping using 2 functions
        1.  linear function that with these points (Q[outlier], y1), (Q[1-outlier], y2)
            Q[outlier] is the outlier percentile of the data
        2.  log function (depends on the output of the first function)
        This class also adapt the linear mapping slowly and give more gravity for new data
        :param epsilon: infinitesimal number to avoid dividing by zero
        """
        self.outlier = outlier
        self.epsilon = epsilon
        self.lr = lr
        self.squash_eps = squash_eps
        self.y1 = y1
        self.y2 = y2
        self.m = None
        self.n = None

    def adapt(self, new_data: Tensor):
        data_size = len(new_data)
        outlier = int(data_size * self.outlier)

        x2, _ = torch.kthvalue(new_data, data_size - outlier, dim=0)
        x1, _ = torch.kthvalue(new_data, outlier, dim=0)

        m = (self.y2 - self.y1) / (x2 - x1 + self.epsilon)
        n = self.y2 - m * x2

        if self.m is None or self.n is None:
            self.m = m
            self.n = n
        else:
            self.m = (1 - self.lr) * self.m + self.lr * m
            self.n = (1 - self.lr) * self.n + self.lr * n

    def map(self, tensor: Tensor):
        if not self.m or not self.n:
            return tensor
        tensor = tensor * self.m + self.n

        x_clamp_up = torch.clamp(tensor, min=self.squash_eps)
        x_clamp_down = torch.clamp(tensor, max=-self.squash_eps)

        x_log_up = torch.log(x_clamp_up) + 1
        x_log_down = -torch.log(-x_clamp_down) - 1
        tensor = (
            x_log_up * (tensor >= 1).float()
            + tensor * (tensor >= -1).float() * (tensor < 1).float()
            + x_log_down * (tensor < -1).float()
        )
        return tensor

    def inverse(self, tensor: Tensor):
        x_clamp_up = torch.clamp(tensor, min=self.squash_eps)
        x_clamp_down = torch.clamp(tensor, max=-self.squash_eps)

        x_exp_up = torch.exp(x_clamp_up - 1)
        x_exp_down = -torch.exp(-(x_clamp_down + 1))

        tensor = (
            x_exp_up * (tensor >= 1).float()
            + tensor * (tensor >= -1).float() * (tensor < 1).float()
            + x_exp_down * (tensor < -1).float()
        )

        tensor = (tensor - self.n) / self.m
        return tensor
