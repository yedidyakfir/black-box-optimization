from torch import Tensor


class InputMapping:
    def map(self, tensor: Tensor):
        raise NotImplementedError()

    def inverse(self, tensor: Tensor):
        raise NotImplementedError()

    def squeeze(self, best_result: Tensor):
        pass


class OutputMapping:
    def map(self, tensor: Tensor):
        raise NotImplementedError()

    def inverse(self, tensor: Tensor):
        raise NotImplementedError()

    def adapt(self, new_data: Tensor):
        pass


class DefaultMapping(OutputMapping):
    def map(self, tensor: Tensor):
        return tensor

    def inverse(self, tensor: Tensor):
        return tensor
