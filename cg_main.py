import functools
import logging
from dataclasses import dataclass
from typing import Union

import numpy as np
from torch.types import Device
from torch.utils.tensorboard import SummaryWriter
from tqdm import tqdm

from convergence_algorithms.conjugate_gradient import ConjugateGradient
from convergence_algorithms.typing import BoundedEvaluatedSpace
from handlers.drawer_handlers import TensorboardDrawerHandler
from handlers.drawers.base_drawer import MultipleDrawerEpoch
from handlers.drawers.convergence_drawer import FullFigConvergenceDrawer, BestModelDrawer
from utils import (
    find_the_most_free_device,
    spaces_index_to_run,
    create_global_handlers,
)


def run_cg(
    space: BoundedEvaluatedSpace,
    config: dataclass,
    device: Union[Device, int] = 0,
    output_bar: bool = True,
    output_graph: bool = True,
):
    dims = len(space.lower_bound)

    cg = ConjugateGradient(space, (np.random.random(dims) - 0.5) * 10)
    writer = SummaryWriter(filename_suffix="conjugate gradient", comment="conjugate gradient")

    if not output_bar:
        tqdm.__init__ = functools.partialmethod(tqdm.__init__, disable=True)

    cg.train(
        callback_handlers=[
            *create_global_handlers("cg", repr(space), writer=writer),
            *(
                [
                    TensorboardDrawerHandler(
                        MultipleDrawerEpoch(
                            [
                                FullFigConvergenceDrawer(dim_size=dims, map_output=False),
                                BestModelDrawer(),
                            ]
                        ),
                        name=f"cg {repr(space)}",
                        writer=writer,
                    ),
                ]
                if output_graph
                else []
            ),
        ],
    )


def main():
    logging.basicConfig(format="%(levelname)s - %(message)s", level=logging.DEBUG)
    for space in spaces_index_to_run():
        run_cg(space, None, find_the_most_free_device(), output_graph=False)


if __name__ == "__main__":
    main()
