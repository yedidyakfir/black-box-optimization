torch
tqdm
mock
pytest
cocopp
tensorboard
matplotlib==3.5.1
numpy
wandb
cma
aenum
OptimizationTestFunctions