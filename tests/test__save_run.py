from pathlib import Path
from unittest.mock import MagicMock

import pytest

from handlers.save_run import SaveMinMaxHandler


@pytest.fixture
def env_path(tmp_path):
    file_path = tmp_path / "coco_min_max"
    file_path.write_text("-135.16,62819756.21238569")
    yield file_path
    file_path.unlink()


@pytest.fixture
def min_max_handler(env_path: Path) -> SaveMinMaxHandler:
    return SaveMinMaxHandler(env_path.name.split("_")[0], env_path.parent)


def test__save_min_max__alg_start__file_exists__load_from_file(min_max_handler):
    # Arrange
    min_max_handler.save_best_worst = MagicMock()
    alg = MagicMock()

    # Act
    min_max_handler.on_algorithm_start(alg)

    # Assert
    min_max_handler.save_best_worst.assert_called_once_with(alg)
    assert min_max_handler.min_point_value == -135.16
    assert min_max_handler.max_point_value == 62819756.21238569


def test__save_min_max__alg_start_file_not_exits__sanity(min_max_handler):
    # Arrange
    alg = MagicMock(environment=MagicMock(return_value=0))

    # Act + Assert
    min_max_handler.on_algorithm_start(alg)


def test__save_min_max__new_min__update(min_max_handler):
    # Arrange
    new_min = -200
    alg = MagicMock(environment=MagicMock(return_value=new_min))

    # Act
    min_max_handler.on_algorithm_start(alg)

    # Assert
    assert alg.environment.call_args_list[0].kwargs["debug_mode"] is True
    assert min_max_handler.min_point_value == new_min


def test__save_min_max__new_max__update(min_max_handler):
    # Arrange
    new_max = 70_000_000
    alg = MagicMock(environment=MagicMock(return_value=new_max))

    # Act
    min_max_handler.on_algorithm_start(alg)

    # Assert
    assert alg.environment.call_args_list[0].kwargs["debug_mode"] is True
    assert min_max_handler.max_point_value == new_max


def test__save_min_max__not_best_point_no_update(min_max_handler):
    # Arrange
    new_point = 0
    alg = MagicMock(environment=MagicMock(return_value=new_point))

    # Act
    min_max_handler.on_algorithm_start(alg)

    # Assert
    assert alg.environment.call_args_list[0].kwargs["debug_mode"] is True
    assert min_max_handler.max_point_value != new_point
    assert min_max_handler.min_point_value != new_point

