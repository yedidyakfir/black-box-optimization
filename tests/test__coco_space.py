import mock
import numpy as np
import pytest
import torch
from mock.mock import MagicMock

import space.coco
from space.coco import CocoSpace
from space.exceptions import NoMoreBudgetError


def test__coco_space__check_budget():
    # Arrange
    budget = 100
    coco_space = CocoSpace(MagicMock(), budget)

    # Assert
    assert coco_space.total_budget == budget


@pytest.mark.parametrize(
    ["data"], [[torch.arange(100).reshape(50, 2)], [np.arange(200).reshape(25, 8)]]
)
def test__coco_space__check_budget_is_raised_after_call(data):
    # Arrange
    coco_space = CocoSpace(MagicMock())

    # Act
    coco_space(data)

    # Assert
    assert coco_space.used_budget == data.shape[0]


@pytest.mark.parametrize(
    ["data"], [[torch.arange(100).reshape(50, 2)], [np.arange(200).reshape(25, 8)]]
)
def test__coco_space__debug_mode__check_budget_is_not_raised_after_call(data):
    # Arrange
    coco_space = CocoSpace(MagicMock())

    # Act
    coco_space(data, debug_mode=True)

    # Assert
    assert coco_space.used_budget == 0


def test__coco_space__raise_error_if_budget_is_over():
    # Arrange
    coco_space = CocoSpace(MagicMock(return_value=10), 1)

    # Act + Assert
    with pytest.raises(NoMoreBudgetError):
        coco_space(torch.arange(20).reshape(10, 2))


def test__coco_space__call__torch_with_gpu():
    # Arrange
    device_num = torch.randint(high=torch.cuda.device_count(), size=(1,))[0]
    if device_num > 0:
        torch.cuda.set_device(device_num)
    device = torch.device("cuda")
    coco_space = CocoSpace(MagicMock(return_value=[10, 2, 3]))

    # Act
    result = coco_space(torch.tensor(1, device=device))

    # Assert
    assert result.device.type == device.type
    assert result.device.index == device_num


@pytest.mark.parametrize(
    ["input_data"],
    [
        (torch.arange(10).reshape(5, 2),),
        (np.arange(10).reshape(5, 2),),
    ],
)
def test__coco_space__call__check_output_shape_and_type(input_data):
    # Arrange
    coco_space = CocoSpace(MagicMock(return_value=10))

    # Act
    point_value = coco_space(input_data)

    # Assert
    assert type(point_value) == type(input_data)
    assert point_value.shape[0] == input_data.shape[0]
    assert len(point_value.shape) == 1


@pytest.mark.parametrize(
    ["input_data"],
    [
        # (torch.tensor([10, 2]),),
        (np.array([10, 2, 3]),),
    ],
)
def test__coco_space__call_single_point__check_output_shape_and_type(input_data):
    # Arrange
    coco_space = CocoSpace(MagicMock(return_value=10))

    # Act
    point_value = coco_space(input_data)

    # Assert
    assert type(point_value) == type(input_data)
    assert len(point_value.shape) == 0


@mock.patch(f"{space.coco.__name__}.torch")
def test__coco_space__sample_from_space__sanity(torch_mock):
    # Arrange
    dim = 4
    coco_space = CocoSpace(
        MagicMock(
            dimension=dim,
            lower_bounds=torch.tensor([1, 4, -1, 22]),
            upper_bounds=torch.tensor([2, 5, 0, 40]),
        ),
    )
    # 1, 1, 1, 18
    torch_mock.from_numpy = lambda x: x
    torch_mock.rand.return_value = torch.arange(16).reshape((dim, 4)) / 16
    expected_result = torch.tensor(
        [
            [1.0000, 4.0625, -0.8750, 25.3750],
            [1.2500, 4.3125, -0.6250, 29.8750],
            [1.5000, 4.5625, -0.3750, 34.3750],
            [1.7500, 4.8125, -0.1250, 38.8750],
        ]
    )

    # Act
    result = coco_space.sample_from_space(4)

    # Assert
    assert torch.all(result == expected_result)


@pytest.mark.parametrize(
    ["lower_bounds", "upper_bounds", "num_samples"],
    [
        [np.array([1, 2, 3]), np.array([4, 3, 5]), 3],
        [np.array([1, 2, 3]), np.array([4, 3, 5]), 10],
        [np.array([1, -2, 2.001]), np.array([1.4, 3, 2.002]), 2],
    ],
)
def test__coco_space__sample_from_space__check_output_in_bounds(
    lower_bounds, upper_bounds, num_samples
):
    # Arrange
    coco_space = CocoSpace(
        MagicMock(
            dimension=len(lower_bounds), lower_bounds=lower_bounds, upper_bounds=upper_bounds
        )
    )

    # Act
    result = coco_space.sample_from_space(num_samples)

    # Assert
    assert torch.all(torch.from_numpy(lower_bounds) <= result)
    assert torch.all(result <= torch.from_numpy(upper_bounds))
