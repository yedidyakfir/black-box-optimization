import torch
from torch import Tensor
from torch.nn import Sequential, Linear, ReLU, Conv2d, Flatten

from convergence_algorithms.modules import BaseSequentialModel


def test__base_sequential_model__reconstruction():
    # Arrange
    seq_model = Sequential(
        Conv2d(3, 10, 3),
        Flatten(),
        Linear(2, 3),
        Linear(3, 4),
        ReLU(),
        Linear(4, 5),
        ReLU(),
    )
    model = BaseSequentialModel(seq_model)

    # Act
    params: Tensor = model.model_parameter_tensor()
    reconstructed_model = model.from_parameter_tensor(params)

    # Assert
    assert torch.all(params == reconstructed_model.model_parameter_tensor())
