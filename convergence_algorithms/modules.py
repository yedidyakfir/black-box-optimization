import copy

import torch
from torch import Tensor
from torch.nn import Module, Sequential, Sigmoid, LeakyReLU, Linear, Tanh


class ConstructableModel(Module):
    def model_parameter_tensor(self):
        model_parameters = []
        for layer in self.children():
            for parameter in layer.parameters():
                model_parameters.append(parameter.reshape(-1))
        return torch.cat(model_parameters).clone()

    def from_parameter_tensor(self, parameter_tensor: Tensor) -> "ConstructableModel":
        raise NotImplementedError()


class BaseSequentialModel(ConstructableModel):
    def __init__(self, sequential_model: Sequential):
        super(BaseSequentialModel, self).__init__()
        self.model = sequential_model

    def forward(self, x):
        return self.model(x)

    def from_parameter_tensor(self, parameter_tensor: Tensor) -> "ConstructableModel":
        parameter_tensor = parameter_tensor.flatten()
        new_model = self.model
        i = 0
        for layer in new_model.children():
            for parameter in layer.parameters():
                parameter.data = (
                    parameter_tensor[i : i + parameter.numel()].clone().reshape(parameter.shape)
                )
                i += parameter.numel()
        return BaseSequentialModel(new_model)


def ada_in_linear(feature, mean_style, std_style, eps=1e-5):
    B, C, H, W = feature.shape
    # batch_size, input_size = feature.shape

    feature = feature.view(B, C, -1)

    std_feat = (torch.std(feature, dim=2) + eps).view(B, C, 1)
    mean_feat = torch.mean(feature, dim=2).view(B, C, 1)

    adain = std_style * (feature - mean_feat) / std_feat + mean_style

    adain = adain.view(B, C, H, W)
    return adain


class LinearResDown(Module):
    def __init__(self, input_size, output_size=None):
        super(LinearResDown, self).__init__()
        if not output_size:
            output_size = input_size // 2
        self.relu1_left = LeakyReLU()
        self.fc1_left = torch.nn.utils.spectral_norm(Linear(input_size, input_size))
        self.relu2_left = LeakyReLU()
        self.fc2_left = torch.nn.utils.spectral_norm(Linear(input_size, input_size))
        self.relu3_left = LeakyReLU()
        self.fc3_left = torch.nn.utils.spectral_norm(Linear(input_size, output_size))

        self.fc1_right = torch.nn.utils.spectral_norm(Linear(input_size, input_size))
        self.fc2_right = torch.nn.utils.spectral_norm(Linear(input_size, output_size))

    def forward(self, batch):
        left_res = self.relu1_left(batch)
        left_res = self.fc1_left(left_res)
        left_res = self.relu2_left(left_res)
        left_res = self.fc2_left(left_res)
        left_res = self.relu3_left(left_res)
        left_res = self.fc3_left(left_res)

        right_res = self.fc1_right(batch)
        right_res = self.fc2_right(right_res)

        return right_res + left_res


class LinearResUP(Module):
    def __init__(self, input_size, scale: int = 2):
        super(LinearResUP, self).__init__()
        output_size = input_size * scale
        self.upsample_left = torch.nn.utils.spectral_norm(Linear(input_size, output_size))
        self.fc_left = torch.nn.utils.spectral_norm(Linear(output_size, output_size))

        self.relu1_right = LeakyReLU()
        self.upsample_right = torch.nn.utils.spectral_norm(Linear(input_size, output_size))
        self.fc1_right = torch.nn.utils.spectral_norm(Linear(output_size, output_size))
        self.relu2_right = LeakyReLU()
        self.fc2_right = torch.nn.utils.spectral_norm(Linear(output_size, output_size))

    def forward(self, batch):
        left_res = self.upsample_left(batch)
        left_res = self.fc_left(left_res)

        right_res = self.relu1_right(batch)
        right_res = self.upsample_right(right_res)
        right_res = self.fc1_right(right_res)
        right_res = self.relu2_right(right_res)
        right_res = self.fc2_right(right_res)

        return right_res + left_res


class GeneratorByDimSize(Module):
    def __init__(self, dim_size: int):
        super(GeneratorByDimSize, self).__init__()
        self.l1 = Linear(1, 50)
        self.l2 = LeakyReLU()
        self.l3 = torch.nn.utils.spectral_norm(Linear(50, 50 * dim_size))
        self.l4 = LeakyReLU()
        self.l5 = torch.nn.utils.spectral_norm(Linear(50 * dim_size, dim_size))
        self.l6 = Tanh()

    def forward(self, batch):
        x = self.l1(batch)
        x = self.l2(x)
        x = self.l3(x)
        x = self.l4(x)
        x = self.l5(x)
        x = self.l6(x)
        return x


class DiscriminatorByInputSize(Module):
    def __init__(self, dim: int):
        super(DiscriminatorByInputSize, self).__init__()
        self.l1 = Linear(dim, 50)
        self.l2 = LeakyReLU()
        self.l3 = torch.nn.utils.spectral_norm(Linear(50, 50))
        self.l4 = LeakyReLU()
        self.l5 = torch.nn.utils.spectral_norm(Linear(50, 1))
        self.l7 = Sigmoid()

    def forward(self, batch):
        x = self.l1(batch)
        x = self.l2(x)
        x = self.l3(x)
        x = self.l4(x)
        x = self.l5(x)
        x = self.l7(x)
        return x
