import copy
import logging
from logging import Logger
from typing import List, Union

import torch
from torch import Tensor
from torch.nn import Module
from torch.optim import Optimizer
from torch.types import Device
from tqdm.auto import trange

from convergence_algorithms.modules import ConstructableModel
from convergence_algorithms.typing import BoundedEvaluatedSpace
from convergence_algorithms.utils import ball_perturb
from handlers.base_handler import AlgorithmCallbackHandler
from handlers.drawers.drawable_algorithms import ConvergenceDrawable
from mapping.base import InputMapping, DefaultMapping, OutputMapping
from space.exceptions import NoMoreBudgetError


class ConvergenceAlgorithm(ConvergenceDrawable):
    def __init__(
        self,
        env: BoundedEvaluatedSpace,
        helper_network: Module,
        model_to_train: ConstructableModel,
        value_optimizer: Optimizer,
        model_to_train_optimizer: Optimizer,
        epsilon: float,
        epsilon_factor: float,
        min_epsilon: float,
        perturb: float,
        max_batch_size: int = 1024,
        num_of_batch_reply: int = 32,
        output_mapping: OutputMapping = None,
        input_mapping: InputMapping = None,
        device: Union[str, Device] = "cpu",
        logger: Logger = logging.getLogger(__name__),
    ):
        self.env = env
        self.helper_network = helper_network
        self.model_to_train = model_to_train
        self.best_model = copy.deepcopy(model_to_train)
        self.helper_optimizer = value_optimizer
        self.model_to_train_optimizer = model_to_train_optimizer
        self.epsilon = epsilon
        self.min_epsilon = min_epsilon
        self.epsilon_factor = epsilon_factor
        self.perturb = perturb
        self.max_batch_size = max_batch_size
        self.num_of_batch_reply = num_of_batch_reply
        self.output_mapping = output_mapping or DefaultMapping()
        self.input_mapping = input_mapping
        self.device = device
        self.logger = logger

    @property
    def best_point_until_now(self):
        return self.best_model.model_parameter_tensor().detach()

    @property
    def environment(self):
        return self.env

    @property
    def curr_point_to_draw(self):
        return self.model_to_train.model_parameter_tensor().detach()

    def train(
        self,
        epochs: int,
        exploration_size: int,
        num_loop_without_improvement: int,
        helper_model_training_epochs: int = 60,
        warmup_minibatch: int = 5,
        callback_handlers: List[AlgorithmCallbackHandler] = None,
    ):
        self.logger.info(f"Starting running {self.__class__.__name__} for {epochs} epochs")
        if not callback_handlers:
            callback_handlers = []

        for callback_handler in callback_handlers:
            callback_handler.on_algorithm_start(self)
        # to prevent error if database is not assigned before exception
        database = torch.tensor([])

        try:
            database, evaluations = self.explore(warmup_minibatch * exploration_size)
            best_model_value = torch.inf
            no_improvement_in_model_count = 0

            for _ in trange(epochs, desc=f"Training EGL {epochs} epochs"):
                # Explore
                samples, new_evaluations = self.explore(exploration_size)
                reply_memory_size = self.num_of_batch_reply * exploration_size
                database = torch.cat((database, samples))[-reply_memory_size:]
                evaluations = torch.cat((evaluations, new_evaluations))[-reply_memory_size:]

                batch_size = min(self.max_batch_size, len(database))
                minibatches = len(database) // batch_size
                self.train_helper_model(
                    database,
                    evaluations,
                    minibatches,
                    batch_size,
                    exploration_size,
                    helper_model_training_epochs,
                )
                self.train_model()

                # Handle end of epoch
                for handler in callback_handlers:
                    handler.on_epoch_end(self, database=database)

                # Check improvement
                real_model = self.model_to_train.model_parameter_tensor()
                if self.input_mapping:
                    real_model = self.env.denormalize(self.input_mapping.inverse(real_model))
                new_model_evaluation = self.env(real_model.cpu().detach())
                if best_model_value > new_model_evaluation:
                    self.logger.info(
                        f"Improved best known point to "
                        f"{self.model_to_train.model_parameter_tensor()}:{best_model_value} "
                        f"From {self.best_model.model_parameter_tensor()}:{new_model_evaluation}"
                    )
                    self.best_model = copy.deepcopy(self.model_to_train)
                    best_model_value = new_model_evaluation
                else:
                    self.logger.warning(
                        f"No improvement for {self.model_to_train.model_parameter_tensor()}:{new_model_evaluation}"
                    )
                    no_improvement_in_model_count += 1

                if no_improvement_in_model_count >= num_loop_without_improvement:
                    no_improvement_in_model_count = 0
                    self.epsilon *= self.epsilon_factor
                    self.epsilon = max(self.epsilon, self.min_epsilon)
                    if self.input_mapping:
                        best_parameters_real = self.input_mapping.inverse(
                            self.best_model.model_parameter_tensor()
                        )
                        self.input_mapping.squeeze(best_parameters_real)
                        self.model_to_train = self.model_to_train.from_parameter_tensor(
                            self.input_mapping.map(best_parameters_real)
                        )
                        self.best_model = copy.deepcopy(self.model_to_train)
                        self.logger.info(
                            f"Shrinking trust region, new center is {best_parameters_real}"
                        )
                    database, evaluations = self.explore(warmup_minibatch * exploration_size)
                    self.logger.info(f"Shrinking sample radius to {self.epsilon}")
                    for handler in callback_handlers:
                        handler.on_algorithm_update(self, database=database)
        except NoMoreBudgetError:
            self.logger.exception("No more Budget")

        for handler in callback_handlers:
            handler.on_algorithm_end(self, database=database)

    def explore(self, exploration_size):
        current_model_parameters = self.model_to_train.model_parameter_tensor()
        self.logger.info(
            f"Exploring around {current_model_parameters}. Sampling {exploration_size} points"
        )
        new_model_samples = torch.cat(
            (
                # TODO - use DI to allow other exploration
                ball_perturb(
                    current_model_parameters, self.epsilon, exploration_size - 1, self.device
                ),
                current_model_parameters.reshape(1, -1),
            )
        )

        if self.input_mapping:
            real_samples = self.input_mapping.inverse(new_model_samples)
            real_samples = self.env.denormalize(
                real_samples
            )  # Map the samples to entire space from (-1, 1)
        else:
            real_samples = new_model_samples

        # Evaluate
        evaluations = torch.FloatTensor(
            [self.env(parameter_sample.cpu().detach()) for parameter_sample in real_samples],
        ).to(self.device)

        self.output_mapping.adapt(evaluations)
        return new_model_samples, evaluations

    def train_helper_model(
        self,
        samples: Tensor,
        samples_value: Tensor,
        num_of_minibatch: int,
        batch_size: int,
        exploration_size: int,
        epochs: int,
    ):
        raise NotImplementedError()

    def train_model(self):
        raise NotImplementedError()
