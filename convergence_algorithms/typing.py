from typing import Protocol

from space.base_space import BoundedSpace, EvaluatedSpace, SamplerSpace, BudgetLimitedSpace


class BoundedSamplerSpace(BoundedSpace, SamplerSpace, Protocol):
    pass


class BoundedEvaluatedSpace(BoundedSpace, EvaluatedSpace, Protocol):
    pass


class BoundedEvaluatedSamplerSpace(
    BoundedSpace, SamplerSpace, EvaluatedSpace, BudgetLimitedSpace, Protocol
):
    pass
