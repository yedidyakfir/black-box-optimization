from typing import Callable

import numpy as np
import torch
from torch import Tensor
from tqdm.auto import trange, tqdm

from convergence_algorithms.convergence import ConvergenceAlgorithm
from convergence_algorithms.utils import ball_perturb


class EGL(ConvergenceAlgorithm):
    def __init__(self, *args, grad_loss: Callable, **kwargs):
        super(EGL, self).__init__(*args, **kwargs)
        self.grad_loss = grad_loss

    @property
    def grad_network(self):
        return self.helper_network

    @property
    def grad_optimizer(self):
        return self.helper_optimizer

    def train_helper_model(
        self,
        samples: Tensor,
        samples_value: Tensor,
        num_of_minibatch: int,
        batch_size: int,
        exploration_size: int,
        epochs: int,
    ):
        self.grad_network.train()
        mapped_evaluations = self.output_mapping.map(samples_value)
        len_replay_buffer = len(mapped_evaluations)
        for _ in trange(
            epochs, leave=False, desc=f"Training the gradient network {epochs} loops"
        ):
            i_indexes = np.random.choice(len_replay_buffer, (num_of_minibatch, batch_size))
            i_reference = np.random.randint(
                0, exploration_size, size=(num_of_minibatch, batch_size)
            )  # with this you calculate the j index

            # We are trying to batch each sample with his corresponding samples,
            # so we refer from num of exploration samples
            explore_indexes = i_indexes // exploration_size

            for reference_index, i_index, explore_index in tqdm(
                zip(i_reference, i_indexes, explore_indexes), leave=False
            ):
                self.grad_optimizer.zero_grad()
                j_index = torch.LongTensor(exploration_size * explore_index + reference_index)
                x_i = samples[i_index]
                x_j = samples[j_index]
                y_i = mapped_evaluations[i_index]
                y_j = mapped_evaluations[j_index]
                x_tag_perturb = ball_perturb(
                    x_i, self.epsilon * self.perturb, batch_size, self.device
                )
                grad_on_perturb = self.grad_network(x_tag_perturb)

                value = ((x_j - x_i) * grad_on_perturb).sum(dim=1)
                target = y_j - y_i

                loss = self.grad_loss(value, target)
                loss.backward()
                self.grad_optimizer.step()
        self.grad_network.eval()

    def train_model(self):
        self.model_to_train.train()
        self.model_to_train_optimizer.zero_grad()
        self.grad_optimizer.zero_grad()
        self.grad_network.eval()

        model_to_train_gradient = self.grad_network(
            self.model_to_train.model_parameter_tensor()
        )
        model_to_train_gradient[model_to_train_gradient != model_to_train_gradient] = 0
        self.logger.info(
            f"Algorithm {self.__class__.__name__} "
            f"moving from {self.model_to_train.model_parameter_tensor()} "
            f"Step size: {torch.norm(model_to_train_gradient)}"
        )

        # Update the gradient
        gradient_index = 0
        for layer in self.model_to_train.children():
            for model_parameter in layer.parameters():
                gradient_offset = model_parameter.numel()
                parameter_gradient = (
                    model_to_train_gradient[gradient_index : gradient_index + gradient_offset]
                    .reshape(model_parameter.shape)
                    .clone()
                )
                with torch.no_grad():
                    model_parameter.grad = parameter_gradient
                gradient_index += gradient_offset

        self.model_to_train_optimizer.step()
        self.model_to_train.eval()
