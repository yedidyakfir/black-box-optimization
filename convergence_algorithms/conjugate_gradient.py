import logging
import math
from logging import Logger
from typing import List

import numpy as np
import torch
from scipy.optimize import minimize

from handlers.base_handler import AlgorithmCallbackHandler
from handlers.drawers.drawable_algorithms import ConvergenceDrawable
from space.base_space import EvaluatedSpace
from space.exceptions import NoMoreBudgetError
from utils import float_range


class ConjugateGradient(ConvergenceDrawable):
    def __init__(
        self,
        space: EvaluatedSpace,
        initial_point: np.array,
        logger: Logger = logging.getLogger(__name__),
    ):
        self.space = space
        self.curr_point = initial_point
        self.logger = logger

    @property
    def best_point_until_now(self):
        return torch.tensor(self.curr_point)

    @property
    def curr_point_to_draw(self):
        return torch.tensor(self.curr_point)

    @property
    def environment(self):
        return self.space

    def train(
        self, epochs: int = math.inf, callback_handlers: List[AlgorithmCallbackHandler] = None
    ):
        self.logger.info(f"Starting cg on {self.space}")
        callback_handlers = callback_handlers or []
        for handler in callback_handlers:
            handler.on_algorithm_start(self)

        def cg_callback(xk):
            self.curr_point = xk
            for c_handler in callback_handlers:
                c_handler.on_epoch_end(self)

        try:
            for _ in float_range(epochs):
                self.logger.info(
                    f"new iteration with best solution {self.best_point_until_now} for {self.space}"
                )
                res = minimize(
                    self.space, self.curr_point, method="CG", jac=False, callback=cg_callback
                )
                self.curr_point = res.x
                for handler in callback_handlers:
                    handler.on_epoch_end(self)
        except NoMoreBudgetError:
            return self.curr_point
        finally:
            for handler in callback_handlers:
                handler.on_algorithm_end(self)
