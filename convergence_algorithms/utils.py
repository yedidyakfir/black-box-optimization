import torch
from torch import Tensor
from torch.nn import Module
from torch.optim import Adam
from torch.types import Device
from torch.utils.data import TensorDataset, RandomSampler, DataLoader


def reset_all_weights(model: Module) -> None:
    """
    refs:
        - https://discuss.pytorch.org/t/how-to-re-set-alll-parameters-in-a-network/20819/6
        - https://stackoverflow.com/questions/63627997/reset-parameters-of-a-neural-network-in-pytorch
        - https://pytorch.org/docs/stable/generated/torch.nn.Module.html
    """

    @torch.no_grad()
    def weight_reset(m: Module):
        reset_parameters = getattr(m, "reset_parameters", None)
        if callable(reset_parameters):
            m.reset_parameters()

    # Applies fn recursively to every submodule see: https://pytorch.org/docs/stable/generated/torch.nn.Module.html
    model.apply(fn=weight_reset)


def ball_perturb(
    ball_center: Tensor, eps: float, num_samples: int, device: str = "cpu"
) -> Tensor:
    ball_dim_size = ball_center.shape[-1]

    perturb = torch.FloatTensor(num_samples, ball_dim_size).to(device=device).normal_()
    mag = torch.FloatTensor(num_samples, 1).to(device=device).uniform_()
    perturb = perturb / (torch.norm(perturb, dim=1, keepdim=True) + 1e-8)

    explore = ball_center + eps * mag * perturb
    return explore


def random_sampler_loader_from_tensor(data: Tensor, batch_size: int, samples: int):
    opt_dataset = TensorDataset(data)
    opt_sampler = RandomSampler(opt_dataset, replacement=True, num_samples=batch_size * samples)
    return DataLoader(opt_dataset, sampler=opt_sampler, batch_size=batch_size)


def wgan_gradient_penalty_loss(
    model: Module,
    generated_data: Tensor,
    real_data: Tensor,
    gradient_penalty_factor: float,
    batch_size: int,
):
    basic_loss = model(generated_data).mean() - model(real_data).mean()
    eps = torch.rand(generated_data.shape, device=generated_data.device)
    interpolation = eps * real_data + (1 - eps) * generated_data
    interp_logits = model(interpolation)
    grad_output = torch.ones_like(interp_logits, device=interp_logits.device)
    gradients = torch.autograd.grad(
        outputs=interp_logits,
        inputs=interpolation,
        grad_outputs=grad_output,
        create_graph=True,
        retain_graph=True,
    )[0]
    gradients = gradients.view(
        batch_size, -1
    )  # we smash all the gradient for each batch together into a single array (relevant in multy dim)
    gradient_penalty = torch.mean((gradients.norm(2, 1) - 1) ** 2)
    return basic_loss + gradient_penalty_factor * gradient_penalty


def default_discriminator_optimizer(model: Module):
    return Adam(model.parameters(), lr=5.0e-3)


def default_generator_optimizer(model: Module):
    return Adam(model.parameters(), lr=1e-4)


def sample_input_to_generator(num_samples: int, device: Device = torch.device("cpu")) -> Tensor:
    input_shape = (num_samples, 1)
    return torch.rand(input_shape, device=device) * 2 - 1
