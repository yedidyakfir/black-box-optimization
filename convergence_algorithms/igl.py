from typing import Callable

import numpy as np
from torch import Tensor
from tqdm.auto import trange, tqdm

from convergence_algorithms.convergence import ConvergenceAlgorithm


class IGL(ConvergenceAlgorithm):
    def __init__(self, *args, loss: Callable, **kwargs):
        super(IGL, self).__init__(*args, **kwargs)
        self.loss = loss

    @property
    def value_network(self):
        return self.helper_network

    @property
    def value_optimizer(self):
        return self.helper_optimizer

    def train_helper_model(
        self,
        samples: Tensor,
        samples_value: Tensor,
        num_of_minibatch: int,
        batch_size: int,
        exploration_size: int,
        epochs: int,
    ):
        self.value_network.train()
        mapped_evaluations = self.output_mapping.map(samples_value)
        len_replay_buffer = len(mapped_evaluations)
        for _ in trange(
            epochs, leave=False, desc=f"Training the gradient network {epochs} loops"
        ):
            i_indexes = np.random.choice(len_replay_buffer, (num_of_minibatch, batch_size))

            for i_index in tqdm(i_indexes, leave=False):
                x_i = samples[i_index]
                y_i = mapped_evaluations[i_index]

                self.value_optimizer.zero_grad()
                self.model_to_train_optimizer.zero_grad()
                predicted_value = self.value_network(x_i)

                loss = self.loss(predicted_value.squeeze(), y_i)
                loss.backward()
                self.value_optimizer.step()
        self.value_network.eval()

    def train_model(self):
        self.model_to_train_optimizer.zero_grad()
        loss = self.value_network(self.model_to_train.model_parameter_tensor())
        loss.backward()
        self.model_to_train_optimizer.step()
        self.logger.info(f"Algorithm {self.__class__.__name__} updated after loss {loss}")
