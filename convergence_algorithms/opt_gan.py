import logging
from logging import Logger
from typing import List

import torch
from torch import Tensor
from torch.nn import Module
from torch.optim import Optimizer
from torch.types import Device
from tqdm.autonotebook import trange, tqdm

from convergence_algorithms.typing import BoundedSamplerSpace
from convergence_algorithms.utils import (
    default_discriminator_optimizer,
    random_sampler_loader_from_tensor,
    sample_input_to_generator,
    wgan_gradient_penalty_loss,
    default_generator_optimizer,
)
from handlers.base_handler import AlgorithmCallbackHandler
from handlers.drawers.drawable_algorithms import ConvergenceDrawable

DEFAULT_BATCH_SIZE = 30


class OptGAN(Module, ConvergenceDrawable):
    def __init__(
        self,
        space: BoundedSamplerSpace,
        generator: Module,
        explore_discriminator: Module,
        exploit_discriminator: Module,
        best_dataset_samples: Tensor,
        gradient_penalty_factor: float = 0.1,
        explore_discriminator_opt: Optimizer = None,
        exploit_discriminator_opt: Optimizer = None,
        generator_opt: Optimizer = None,
        discriminator_iter: int = 4,
        generator_iter: int = 150,
        ee_factor: float = 0.3,
        device: Device = torch.device("cpu"),
        logger: Logger = logging.getLogger(__name__),
    ):
        super(OptGAN, self).__init__()
        self.space = space
        self.generator = generator
        self.explore_discriminator = explore_discriminator
        self.exploit_discriminator = exploit_discriminator
        self.gradient_penalty_factor = gradient_penalty_factor
        self.explore_discriminator_opt = (
            explore_discriminator_opt or default_discriminator_optimizer(explore_discriminator)
        )
        self.exploit_discriminator_opt = (
            exploit_discriminator_opt or default_discriminator_optimizer(exploit_discriminator)
        )
        self.generator_opt = generator_opt or default_generator_optimizer(generator)
        self.best_dataset_samples = best_dataset_samples
        self.discriminator_iter = discriminator_iter
        self.generator_iter = generator_iter
        self.ee_factor = ee_factor
        self.device = device
        self.logger = logger

    @property
    def best_point_until_now(self):
        return self.curr_point_to_draw

    @property
    def curr_point_to_draw(self):
        return self.space.best_k_values(
            self.best_dataset_samples.cpu().detach(), 1, debug_mode=True
        ).squeeze()

    @property
    def environment(self):
        return self.space

    def forward(self, batch: Tensor):
        return self.query_generator_in_space(batch)

    def query_generator_in_space(self, batch: Tensor):
        return self.space.denormalize(self.generator(batch))

    def train(
        self,
        epochs: int = 20,
        m: int = 30,
        a: float = 1.5,
        k_0: int = 150,
        max_fes: int = 5_000,
        batch_size: int = DEFAULT_BATCH_SIZE,
        callback_handlers: List[AlgorithmCallbackHandler] = None,
        *args,
        **kwargs,
    ):
        self.logger.info(f"Started running {self.__class__.__name__}")
        callback_handlers = callback_handlers or []

        for handler in callback_handlers:
            handler.on_algorithm_start(self, database=self.best_dataset_samples)

        self.pre_iter_training(
            *args, batch_size=batch_size, callback_handlers=callback_handlers, **kwargs
        )
        self.distribution_reshaping(epochs, m, a, k_0, max_fes, batch_size, callback_handlers)
        self.logger.info(f"Finish running, best value is {self.best_point_until_now}")

        # Finish with handlers
        for handler in callback_handlers:
            handler.on_algorithm_end(self, database=self.best_dataset_samples)

    def distribution_reshaping(
        self,
        epochs: int,
        m: int,
        a: float,
        k_0: int,
        max_fes: int,
        batch_size: int,
        callback_handlers: List[AlgorithmCallbackHandler] = None,
    ):
        self.logger.info(f"Started distribution reshaping for {epochs} epochs")
        optimum_values_loader = random_sampler_loader_from_tensor(
            self.best_dataset_samples, batch_size, self.generator_iter * self.discriminator_iter
        )
        for epoch in trange(epochs, position=0, leave=False):
            for i, batch in tqdm(enumerate(optimum_values_loader), position=1, leave=False):
                self.explore_discriminator.train()
                self.exploit_discriminator.train()
                self.generator.eval()

                self.explore_discriminator.zero_grad()
                self.exploit_discriminator.zero_grad()
                self.generator.zero_grad()
                noise = sample_input_to_generator(batch_size, device=self.device)
                explore_generator_output = self.query_generator_in_space(noise)
                exploit_generator_output = self.query_generator_in_space(noise)
                x_uniform = self.space.sample_from_space(batch_size, device=self.device)
                x_sampling = batch[0]
                explore_disc_loss = wgan_gradient_penalty_loss(
                    self.explore_discriminator,
                    explore_generator_output,
                    x_uniform,
                    self.gradient_penalty_factor,
                    batch_size,
                )
                exploit_disc_loss = wgan_gradient_penalty_loss(
                    self.exploit_discriminator,
                    exploit_generator_output,
                    x_sampling,
                    self.gradient_penalty_factor,
                    batch_size,
                )
                total_disc_loss = explore_disc_loss + exploit_disc_loss
                total_disc_loss.backward()
                self.explore_discriminator_opt.step()
                self.exploit_discriminator_opt.step()

                if i % self.discriminator_iter:
                    self.logger.info(
                        f"Improve generator after {self.discriminator_iter} iteration"
                    )
                    self.generator.train()
                    self.explore_discriminator.eval()
                    self.exploit_discriminator.eval()

                    self.generator.zero_grad()
                    self.exploit_discriminator.zero_grad()
                    self.explore_discriminator.zero_grad()
                    noise = sample_input_to_generator(batch_size, device=self.device)
                    generated_output = self.query_generator_in_space(noise)

                    generator_loss = -(
                        (1 / (1 + self.ee_factor))
                        * self.exploit_discriminator(generated_output)
                        + (self.ee_factor / (1 + self.ee_factor))
                        * self.explore_discriminator(generated_output)
                    ).mean()
                    generator_loss.backward()
                    self.generator_opt.step()

                    self.generator.eval()
            noise = sample_input_to_generator(m, device=self.device)
            x_g = self.query_generator_in_space(noise)
            new_x_opt = torch.cat((x_g, self.best_dataset_samples))

            # shrink
            self.logger.info(f"Shrinking database with k0: {k_0}, max fes: {max_fes}")
            new_size = min(int(k_0 ** (1 - (a * epoch / max_fes))), new_x_opt.shape[0])

            self.best_dataset_samples = self.space.best_k_values(new_x_opt.detach(), new_size)
            optimum_values_loader = random_sampler_loader_from_tensor(
                self.best_dataset_samples,
                batch_size,
                self.generator_iter * self.discriminator_iter,
            )

            # Send to handler
            for handler in callback_handlers:
                handler.on_epoch_end(self, database=self.best_dataset_samples)

    def pre_iter_training(
        self,
        pre_iter_loops: int = 20,
        batch_size: int = DEFAULT_BATCH_SIZE,
        callback_handlers: List[AlgorithmCallbackHandler] = None,
    ):
        self.logger.info(f"Started pre training for {pre_iter_loops} loops")
        for _ in trange(pre_iter_loops, leave=False, position=0, desc="Loops"):
            for _ in trange(
                self.generator_iter, leave=False, position=1, desc="Generator iteration"
            ):
                self.explore_discriminator.train()
                self.generator.eval()
                for _ in trange(
                    self.discriminator_iter,
                    leave=False,
                    position=2,
                    desc="Discriminator iteration",
                ):
                    self.explore_discriminator_opt.zero_grad()
                    gen_random_input = sample_input_to_generator(batch_size, device=self.device)
                    x_g = self.query_generator_in_space(gen_random_input)
                    x_uniform = self.space.sample_from_space(batch_size, device=self.device)
                    loss = wgan_gradient_penalty_loss(
                        self.explore_discriminator,
                        x_g,
                        x_uniform,
                        self.gradient_penalty_factor,
                        batch_size,
                    )
                    loss.backward()
                    self.explore_discriminator_opt.step()
                self.generator.train()
                self.explore_discriminator.eval()
                self.generator_opt.zero_grad()
                gen_random_input = sample_input_to_generator(batch_size, device=self.device)
                explore_disc_output = self.explore_discriminator(
                    self.query_generator_in_space(gen_random_input)
                )
                gen_loss = -explore_disc_output.mean()
                gen_loss.backward()
                self.generator_opt.step()

            # Send to handler
            for handler in callback_handlers:
                handler.on_epoch_end(self, database=self.best_dataset_samples)

    def sample_from_best_samples(self, n_samples: int = 1):
        index_permutations = torch.randperm(len(self.best_dataset_samples))
        samples_idx = index_permutations[:n_samples]
        return self.best_dataset_samples[samples_idx]
