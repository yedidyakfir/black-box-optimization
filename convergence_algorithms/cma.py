import logging
from logging import Logger
from typing import List, Protocol

import numpy as np
import torch
from cma import CMAEvolutionStrategy

from handlers.base_handler import AlgorithmCallbackHandler
from handlers.drawers.drawable_algorithms import ConvergenceDrawable
from space.base_space import EvaluatedSpace
from space.exceptions import NoMoreBudgetError


class CMA(ConvergenceDrawable):
    @property
    def best_point_until_now(self):
        return torch.tensor(self.curr_best_point)

    def __init__(
        self,
        cma_es: CMAEvolutionStrategy,
        space: EvaluatedSpace,
        initial_point: np.ndarray,
        logger: Logger = logging.getLogger(__name__),
    ):
        self.curr_best_point = initial_point
        self.cma_es = cma_es
        self.space = space
        self.logger = logger

    @property
    def curr_point_to_draw(self):
        return torch.tensor(self.curr_best_point)

    @property
    def environment(self):
        return self.space

    @classmethod
    def from_coco_func(cls, space: EvaluatedSpace, dim):
        return cls(CMAEvolutionStrategy(dim * [0], 0.5), space, np.array([5] * dim))

    def _calculate_best_point(self, solutions):
        solutions_values = {tuple(x): self.space(x, debug_mode=True) for x in solutions}
        solutions_values.update(
            {tuple(self.curr_best_point): self.space(self.curr_best_point, debug_mode=True)}
        )
        return np.array(min(solutions_values, key=solutions_values.get))

    def train(self, callback_handlers: List[AlgorithmCallbackHandler] = None):
        self.logger.info(f"starting algorithm cma for space {self.space}")
        callback_handlers = callback_handlers or []
        for handler in callback_handlers:
            handler.on_algorithm_start(self)

        try:
            while not self.cma_es.stop():
                self.logger.info(f"new iteration with best solution {self.curr_best_point} for {self.space}")
                solutions = np.array(self.cma_es.ask())
                solutions_value = self.space(solutions)
                self.cma_es.tell(solutions, solutions_value)
                self.cma_es.logger.add()  # write data to disc to be plotted
                self.cma_es.disp()
                self.curr_best_point = self._calculate_best_point(solutions)
                for handler in callback_handlers:
                    handler.on_epoch_end(self, database=torch.tensor(solutions))
        except NoMoreBudgetError:
            self.logger.warning("Exceeded budget")
        finally:
            for handler in callback_handlers:
                handler.on_algorithm_end(self)
