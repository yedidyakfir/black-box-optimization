import functools
import math
import os
from pathlib import Path
from typing import Tuple, List

from handlers.base_handler import AlgorithmCallbackHandler
from handlers.drawers.base_drawer import Drawer
from handlers.drawers.drawable_algorithms import ConvergenceDrawable


def min_max_result_file_path(dir_path: Path, alg_name: str) -> Path:
    return dir_path / f"{alg_name}_min_max"


class SaveMinMaxHandler(AlgorithmCallbackHandler):
    def __init__(self, env_name: str, file_path: Path = Path() / "results" / "min_max"):
        self.file_path = file_path
        self.env_name = env_name
        self.min_point_value = math.inf
        self.max_point_value = -math.inf

    @property
    def result_path(self):
        self.file_path.mkdir(parents=True, exist_ok=True)
        return min_max_result_file_path(self.file_path, self.env_name)

    def save_best_worst(self, alg: ConvergenceDrawable):
        current_point_value = alg.environment(
            alg.curr_point_to_draw.cpu().detach(), debug_mode=True
        )
        if self.max_point_value < current_point_value:
            self.max_point_value = current_point_value
        if self.min_point_value > current_point_value:
            self.min_point_value = current_point_value

    def on_algorithm_start(self, alg: ConvergenceDrawable, *args, **kwargs):
        if self.result_path.exists():
            min_point_value, max_point_value = self.result_path.read_text().split(",")
            self.min_point_value, self.max_point_value = float(min_point_value), float(
                max_point_value
            )
        self.save_best_worst(alg)

    def on_epoch_end(self, alg, *args, **kwargs):
        self.save_best_worst(alg)

    def on_algorithm_update(self, alg, *args, **kwargs):
        self.save_best_worst(alg)

    def on_algorithm_end(self, alg, *args, **kwargs):
        self.result_path.write_text(f"{self.min_point_value},{self.max_point_value}")


class SaveRunToFileHandler(AlgorithmCallbackHandler):
    def __init__(
        self,
        drawer: Drawer,
        env_name: str,
        alg_name: str,
        file_path: Path = Path() / "results" / "budget",
    ):
        super(SaveRunToFileHandler, self).__init__()
        self.drawer = drawer
        self.file_path = file_path
        self.alg_name = alg_name
        self.env_name = env_name
        self.counter = 0

    def on_algorithm_start(self, alg: ConvergenceDrawable, *args, **kwargs):
        drawing = self.drawer.start_drawing(alg, *args, **kwargs)
        self.handle_drawings(drawing, "w")

    def on_epoch_end(self, alg: ConvergenceDrawable, *args, **kwargs):
        drawing = self.drawer.update_data(alg, *args, **kwargs)
        self.handle_drawings(drawing)

    def on_algorithm_update(self, alg, *args, **kwargs):
        drawing = self.drawer.update_data(alg, *args, **kwargs)
        self.handle_drawings(drawing)

    def on_algorithm_end(self, alg: ConvergenceDrawable, *args, **kwargs):
        drawing = self.drawer.draw_data(alg, *args, **kwargs)
        self.handle_drawings(drawing)

    def handle_drawings(self, drawings, how_to_open_file: str = "a"):
        for drawing, drawing_name in drawings:
            self.handle_drawing(drawing, drawing_name, how_to_open_file)
        self.counter += 1

    @functools.singledispatchmethod
    def handle_drawing(self, drawing_data, drawing_name: str, how_to_open_file: str):
        raise NotImplementedError()

    @handle_drawing.register(tuple)
    def handle_scalar(
        self, drawing_data: Tuple[float, int], drawing_name: str, how_to_open_file: str
    ):
        func_value, budget = drawing_data
        self.file_path.mkdir(parents=True, exist_ok=True)
        result_file_path = self.file_path / f"{self.alg_name}-{self.env_name}"
        with result_file_path.open(how_to_open_file) as result_file:
            result_file.write(f"{budget},{func_value}{os.linesep}")


class MeanResultCalculater:
    def __init__(self, result_path: Path, min_max_path: Path, algorithm_name: str):
        self.result_path = result_path
        self.min_max_path = min_max_path
        self.algorithm_name = algorithm_name

    def calc_mean(self) -> List[Tuple[float, int]]:
        algorithm_result_file_names = [
            file
            for file in self.result_path.iterdir()
            if file.name.startswith(self.algorithm_name)
        ]
        if not algorithm_result_file_names:
            return []
        algorithm_mapped_result = {}
        for result_file_name in algorithm_result_file_names:
            env_name = str.partition(result_file_name.name, "-")[-1]
            min_max_result_path = min_max_result_file_path(self.min_max_path, env_name)
            min_point_value, max_point_value = min_max_result_path.read_text().split(",")
            algorithm_min, algorithm_max = float(min_point_value), float(max_point_value)

            alg_result = [
                stripped_line
                for line in result_file_name.read_text().splitlines()
                if (stripped_line := line.strip())
            ]
            mapped_alg_result = {}
            for line in alg_result:
                budget, func_value = line.strip().split(",")
                budget, func_value = int(budget), float(func_value)
                mapped_alg_result[budget] = (func_value - algorithm_min) / (
                    algorithm_max - algorithm_min
                )
            algorithm_mapped_result[env_name] = mapped_alg_result

        max_steps = max(
            [max(alg_result.keys()) for alg_result in algorithm_mapped_result.values()]
        )
        total_alg_results = {alg: [] for alg in algorithm_mapped_result.keys()}

        for i in range(max_steps):
            for alg, alg_result in algorithm_mapped_result.items():
                if i in alg_result:
                    total_alg_results[alg] += [alg_result[i]]
                else:
                    try:
                        total_alg_results[alg] += [total_alg_results[alg][i - 1]]
                    except IndexError:
                        min_key_result = alg_result[min(alg_result.keys())]
                        total_alg_results[alg] = [min_key_result]

        total_result = {budget: [] for budget in range(max_steps)}

        for i in range(max_steps):
            for alg_result in total_alg_results.values():
                total_result[i] += [alg_result[i]]

        return [
            (sum(alg_results) / len(alg_results), budget)
            for budget, alg_results in total_result.items()
        ]
