import abc
from abc import ABC


class ConvergenceDrawable(ABC):
    @property
    @abc.abstractmethod
    def curr_point_to_draw(self):
        raise NotImplementedError()

    @property
    @abc.abstractmethod
    def environment(self):
        raise NotImplementedError()

    @property
    @abc.abstractmethod
    def best_point_until_now(self):
        raise NotImplementedError()
