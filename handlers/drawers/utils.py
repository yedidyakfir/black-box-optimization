import functools
from typing import Tuple

import torch
from torch import Tensor
from torch.nn.functional import pad
from torch.types import Device

from convergence_algorithms.convergence import ConvergenceAlgorithm
from handlers.drawers.base_drawer import Drawer


def pad_2d_to_nd_graph(
    x_grid: Tensor, y_grid: Tensor, dims: Tuple[int, int], n: int, pad_value: int = 0
) -> Tensor:
    padded_points_until_second_dim = pad(
        x_grid.reshape(-1, 1), (dims[0], dims[1] - dims[0] - 1), value=pad_value
    )
    points_padded_including_second_dim = torch.cat(
        [padded_points_until_second_dim, y_grid.reshape(-1, 1)], dim=1
    )
    points_on_full_dim = pad(
        points_padded_including_second_dim, (0, n - dims[1] - 1), value=pad_value
    )
    return points_on_full_dim


def create_grid_points(
    lower_bounds: int,
    upper_bounds: int,
    num_of_points: int,
    dim_size: int,
    dims: list,
    device: Device = torch.device("cpu"),
) -> Tensor:
    x_axis = torch.linspace(lower_bounds, upper_bounds, num_of_points, device=device)
    y_axis = torch.linspace(lower_bounds, upper_bounds, num_of_points, device=device)
    x_grid, y_grid = torch.meshgrid(x_axis, y_axis)
    points = pad_2d_to_nd_graph(x_grid, y_grid, tuple(dims), dim_size)
    return points


def map_parameter_to_real(func):
    @functools.wraps(func)
    def map_to_real_wrapper(alg: ConvergenceAlgorithm, *args, **kwargs):
        # Map parameters
        if not alg.input_mapping:
            return func(alg, *args, **kwargs)
        for parameter_name, parameter_value in kwargs.items():
            if isinstance(parameter_value, Tensor):
                kwargs[parameter_name] = alg.env.denormalize(
                    alg.input_mapping.inverse(parameter_value)
                )

        return func(alg, *args, **kwargs)

    return map_to_real_wrapper


def map_to_real(func):
    @functools.wraps(func)
    def map_to_real_wrapper(alg: ConvergenceAlgorithm, *args, **kwargs):
        # Map return value
        points = func(alg, *args, **kwargs)
        if alg.input_mapping:
            points = alg.input_mapping.inverse(points)
            points = alg.env.denormalize(points)
        return points

    return map_to_real_wrapper


def convert_to_real_drawer(drawer: Drawer):
    drawer_callables = [
        method_name for method_name in dir(drawer) if callable(getattr(drawer, method_name))
    ]
    points_generator_callables = [
        method_name
        for method_name in drawer_callables
        if "points" in method_name or "point" in method_name
    ]

    # Convert points function
    for points_gen_method_name in points_generator_callables:
        point_gen_method = getattr(drawer, points_gen_method_name)
        mapped_points_gen_method = map_to_real(map_parameter_to_real(point_gen_method))
        setattr(drawer, points_gen_method_name, mapped_points_gen_method)

    drawer.draw_data = map_parameter_to_real(drawer.draw_data)
    drawer.update_data = map_parameter_to_real(drawer.update_data)
    drawer.start_drawing = map_parameter_to_real(drawer.start_drawing)
    drawer.end_drawing = map_parameter_to_real(drawer.end_drawing)
    return drawer
